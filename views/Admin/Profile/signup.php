<?php
include_once('../../../vendor/autoload.php');

if(!isset($_SESSION) )session_start();
use App\BITM\SEIP\Message\Message;


?>



<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Log In & Sign Up</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  
      <link rel="stylesheet" href="../../../resource/assets/css/adminlogin.css">

  
</head>

<body>

<h1 style="color: #0f0f0f">Daily Expense Tracker System</h1>
<center><table>
    <tr>
        <td width='230' >

        <td width='600' height="50" >


            <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                <div  id="message" class="form button"   style="font-size: smaller;text-align: center  " >
                    <center>
                        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                            echo "&nbsp;ly".Message::message();
                        }
                        Message::message(NULL);
                        ?>
                </div>
            <?php } ?>
        </td>
    </tr>
</table></center>
  <div class="form">
      
      <ul class="tab-group">
        <li class="tab active"><a href="#login">Log In</a></li>
        <li class="tab"><a href="signuppp.php">Sign Up</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="login">   
          <h1>User Log In</h1>


            <form role="form" action="../Authentication/login.php" method="post" class="login-form">
          
            <div class="field-wrap">
                Email Address<span class="req">*</span>
            <input type="email" name="email" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">

              Password<span class="req">*</span>

            <input type="password" name="password" required autocomplete="off"/>
          </div>
          
          <p class="forgot"><a href="forgotten.php">Forgot Password?</a></p>
          
          <button class="button button-block"/>Log In</button>
          
          </form>

        </div>
        
        <div id="signup">
            <form role="form" action="registration.php" method="post" class="registration-form">

                <div class="field-wrap">
                    <label>
                        User Name<span class="req">*</span>
                    </label>
                    <input type="text" name="admin_name" required autocomplete="off"/>
                </div>


          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" name="email" required autocomplete="off"/>
          </div>

                <div class="field-wrap">
                    <label>
                        Phone<span class="req">*</span>
                    </label>
                    <input type="number" name="phone"required autocomplete="off"/>
                </div>
                <div class="field-wrap">
                    <label>
                        Address<span class="req">*</span>
                    </label>
                    <input type="text" name="address" required autocomplete="off"/>
                </div>

          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" name="admin_password" required autocomplete="off"/>
          </div>
          
          <button type="submit" class="button button-block"/>Sign Up</button>
          
          </form>		
         

        </div>
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
  <script src='../../../resource/assets/http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="../../../resource/assets/js/adminlogin.js"></script>

</body>
</html>
