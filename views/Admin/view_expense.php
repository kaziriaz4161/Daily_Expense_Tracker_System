<?php
require_once("../../vendor/autoload.php");

$objAdmin = new App\BITM\SEIP\Admin\Admin();

$allData = $objAdmin->indexExpense();

use App\BITM\SEIP\Message;
use App\BITM\SEIP\Utility\Utility;

if(!isset($_SESSION)){
    session_start();

}
//$msg = Message::getMessage();




?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }




    </style>









</head>
<body class="background">




<!-- required for search, block 4 of 5 end -->
<br><br>
<div class="container">







    <div class="container" style="background: lightseagreen; width: 100%">

        <h1 style="text-align: center;color: #ffffff" ;"><b>Expense Info (<?php echo count($allData) ?>)</b></h1>
        <h2


    </div>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>


            <th style='width: 10%; text-align: center'>Serial Number</th>

            <th>Transaction ID</th>
            <th>UID</th>
            <th>Amount</th>
            <th>Source</th>
        </tr>

        <?php
        $serial= 1;  ##### We need to remove this line to work pagination correctly.


        foreach($allData as $oneData){ ########### Traversing $someData is Required for pagination  #############

            if($serial%2) $bgColor = "AZURE";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>



                     <td style='width: 10%; text-align: center'>$serial</td>

                     <td>$oneData->transaction_id</td>
                     <td>$oneData->Uid</td>
                     <td>$oneData->amount</td>
                     <td>$oneData->source</td>


                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>










</div>


<script>
    jQuery(function($) {
        $('#message').fadeIn(500);
        $('#message').fadeOut (500);
        $('#message').fadeIn (500);
        $('#message').delay (2500);
        $('#message').fadeOut (2000);
    })

    $('#delete').on('click',function(){
        document.forms[1].action="deletemultiple.php";
        $('#multiple').submit();
    });



    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });







</script>







<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->





</body>
</html>