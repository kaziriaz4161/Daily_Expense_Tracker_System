<?php
include_once('../../vendor/autoload.php');

if(!isset($_SESSION) )session_start();
use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Admin\Admin;
use App\BITM\SEIP\Admin\Auth;
use App\BITM\SEIP\Admin\User;

if(!isset($_SESSION)){
    session_start();

}
$msg = Message::getMessage();


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Expense</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../../../resource/assets/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../resource/assets/ico/apple-touch-icon-57-precomposed.png">
    <style>
        #content {
            width: 700px ;
            margin-left: auto ;
            margin-right: auto ;
        }
    </style>

</head>

<body style="align-content: center">
<div><?php echo $msg;?></div>
<h1 class="text-center" style="color: black"><u><b>Add Expense</b></u></h1>
<br>
<div id="content" style="align-content: center;">

    <div class="form-box" style="margin-top: 0%">
        <div class="form-top">

        </div>
        <div class="form-bottom">
            <form role="form" action="store_expense.php" method="post" class="registration-form">
                <div class="form-group">
                    <label class="sr-only" for="form-first_name">Transaction ID</label>
                    <input type="text" name="transaction_id" placeholder="Transaction ID..." class="form-first-name form-control" id="form-first-name">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="form-email">UID</label>
                    <input type="number" name="Uid" placeholder="UID..." class="form-email form-control" id="form-email">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="form-password">Amount</label>
                    <input type="number" name="amount" placeholder="amount..." class="form-password form-control" id="form-password">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-email">Source</label>
                    <input type="text" name="source" placeholder="source..." class="form-phone form-control" id="form-phone">
                </div>

                <button type="submit" class="btn">Submit</button>



        </div>
    </div><br>
    <a href="view_expense.php" class="btn btn-success">View Expense List</a>
    <a href="add_income.php" class="btn btn-success">Add Income</a>
    <a href="view.php" class="btn btn-success">View Income List</a>

</body>
