-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2017 at 11:55 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prototype_tracker_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(15) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `Uid` int(15) NOT NULL,
  `amount` int(255) NOT NULL,
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `expense`
--

TRUNCATE TABLE `expense`;
--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `transaction_id`, `Uid`, `amount`, `source`) VALUES
(1, '1234', 1234, 9000, 'Dalali'),
(2, '1234', 1234, 5678, 'tution'),
(3, '123455', 12345, 30000, 'Business');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` int(15) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `Uid` int(15) NOT NULL,
  `amount` int(255) NOT NULL,
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `income`
--

TRUNCATE TABLE `income`;
--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `transaction_id`, `Uid`, `amount`, `source`) VALUES
(1, '45646', 45646, 0, 'salary'),
(2, '123', 456, 789, 'salary'),
(3, '456', 789, 456, 'salary'),
(4, '564564645', 4656546, 4564545, 'asfafsasf'),
(5, '1234', 123, 5666, 'tution'),
(6, '122', 1111, 1111, 'eeeee'),
(7, '1233', 333, 3333, 'rrrrr'),
(8, '1233', 3333, 333, '3333'),
(9, '1233', 3333, 333, '3333'),
(10, '233', 3333, 4444, 'rrrrrrr'),
(11, '123456', 12345, 100000, 'Business');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `Address` varchar(20) NOT NULL,
  `Uid` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `gender`, `contact`, `Address`, `Uid`) VALUES
(2, 'kaniz\r\n\r\n\r\n\r\nkaniz', 'fatimakaniz7777@gmail.com', '123456', 'female', '01846028856', 'Ctg', 10224),
(3, 'shakil', 'shakil@com', '123456', 'Male', '00555', 'DHaka', 10666),
(4, 'ass', 'shakilf55@gmail.com', '456564', 'Mla', '1351564', 'afsfas', 102),
(5, 'sabrina', 'sabrinacse14@gmail.com', '1234567', 'Female', '01515699626', 'Chittagong', 12345);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
