<?php

namespace App\BITM\SEIP\Admin;

use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;

use App\BITM\SEIP\Model\Database as DB;
use PDO;
use PDOException;

class Admin extends DB
{
    private $transaction_id;
    private $Uid;
    private $amount;
    private $source;


    public function setData($postData){

        if(array_key_exists('transaction_id',$postData)){
            $this->transaction_id = $postData['transaction_id'];
        }

        if(array_key_exists('amount',$postData)){
            $this->amount = $postData['amount'];
        }

        if(array_key_exists('Uid',$postData)){
            $this->Uid = $postData['Uid'];
        }

        if(array_key_exists('source',$postData)){
            $this->source = $postData['source'];
        }



    }


    public function store(){

        //Utility::dd($_POST);
        $arrData = array($this->transaction_id,$this->amount,$this->Uid,$this->source);



        $sql = "INSERT into income(transaction_id,amount,Uid,source) VALUES(?,?,?,?)";



        $STH = $this->conn->prepare($sql);


        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");


       // $_SESSION=$_POST;

        Utility::redirect('add_income.php');


    }


    public function store_expense(){

        //Utility::dd($_POST);
        $arrData = array($this->transaction_id,$this->amount,$this->Uid,$this->source);



        $sql = "INSERT into expense(transaction_id,amount,Uid,source) VALUES(?,?,?,?)";



        $STH = $this->conn->prepare($sql);


        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");


        // $_SESSION=$_POST;

        Utility::redirect('add_expense.php');


    }


    public function index(){

        $sql = "select * from income";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function indexExpense(){

        $sql = "select * from expense";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


}